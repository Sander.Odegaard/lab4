package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] cellStates;

    public CellGrid(int rows, int columns, CellState initialState) {
		
        this.rows = rows;
        this.cols = columns;
        cellStates = new CellState[rows][columns];

        for(int i = 0; i<rows; i++){
            for(int j = 0; j<columns; j++){
                cellStates[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {

        return this.rows;
    }

    @Override
    public int numColumns() {

        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        
        if( row < numRows() && row >= 0 && column < numColumns() && column >= 0 ){
            cellStates[row][column] = element;
        }
        else{
            throw new IndexOutOfBoundsException();
        }
        
    }

    @Override
    public CellState get(int row, int column) {
        if( row < numRows() && row >= 0 && column < numColumns() && column >= 0 ){
            return cellStates[row][column];
        }
        else{
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public IGrid copy() {
        IGrid copyOfGrid = new CellGrid(this.rows, this.cols, null);
        for(int i = 0; i < rows; i++) {
    		for (int j = 0; j < cols; j++) {
    			copyOfGrid.set(i, j, get(i, j));
            }
        }
        return copyOfGrid;
    }
}
    
    
